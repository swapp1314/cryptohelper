package it.unito.di.cryptoware.cryptohelper;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalcolatoreMappaturaTest {

    /**
     * Test of create method, of class CalcolatoreMappatura.
     */
    @Test
    public void testCreate() throws Exception {
        System.out.println("CalcolatoreMappaturaJUnit4Test:  create");
        String metodo[] = {"Cesare","Pseudocasuale","Parola chiave"};
        CalcolatoreMappatura[] expResult = {new CalcolatoreCesare(), new CalcolatorePseudocasuale(),
            new CalcolatoreParolachiave()};
        for(int i=0; i<metodo.length; i++) {
            CalcolatoreMappatura result = CalcolatoreMappatura.create(metodo[i]);
            assertTrue(result.getClass().isInstance(expResult[i]));
        }
    }
}
