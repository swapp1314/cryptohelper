package it.unito.di.cryptoware.cryptohelper;

import org.junit.Test;
import static org.junit.Assert.*;

public class MappaturaTest {
    // yrqsecilagtnoukhpbwmvjxzdf
    private final static char mappa[] = {'y','r','q','s','e','c','i','l','a','g','t','n','o','u','k','h','p','b','w','m','v','j','x','z','d','f'};
    // irfyezjpgvohtlmqcbdknuswax
    private final static char mappaInversa[] = {'i','r','f','y','e','z','j','p','g','v','o','h','t','l','m','q','c','b','d','k','n','u','s','w','a','x'};

    /**
     * Test of map method, of class Mappatura.
     */
    @Test
    public void testMap() {
        System.out.println("MappaturaJunit4Test: map");
        Mappatura m = new Mappatura(mappa);
        for(int i=0; i<mappaInversa.length; i++) {
            assertTrue(m.map(mappaInversa[i]) == (char)('a'+i));
        }
    }

    /**
     * Test of inverseMap method, of class Mappatura.
     */
    @Test
    public void testInverseMap() {
        System.out.println("MappaturaJunit4Test: inverseMap");
        Mappatura m = new Mappatura(mappa);
        for(int i=0; i<mappa.length; i++) {
            assertTrue(m.inverseMap(mappa[i]) == (char)('a'+i));
        }
    }
    
}
