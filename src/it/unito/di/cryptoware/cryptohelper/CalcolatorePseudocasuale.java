package it.unito.di.cryptoware.cryptohelper;

import java.util.Random;

public class CalcolatorePseudocasuale extends CalcolatoreMappatura {
    @Override
    public Mappatura calcola(String chiave) {
        char mappa[] = new char[26];
        Random gen = new Random(Long.parseLong(chiave));
        for(int i=0; i<mappa.length; i++) {
            int pos = gen.nextInt(26);
            /* ciclo di controllo caratteri già mappati */
            while(mappa[pos] != 0) {
                pos = (pos+1)%26;  // già mappato, utilizzo il prossimo disponibile
            }
            mappa[pos] = (char)('a'+i); 
        }
        return new Mappatura(mappa);
    }
}