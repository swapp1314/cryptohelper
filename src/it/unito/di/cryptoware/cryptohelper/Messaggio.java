package it.unito.di.cryptoware.cryptohelper;

import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;

public class Messaggio implements MessaggioMittente, MessaggioDestinatario {
	private int id;
	private String testo, testoCifrato, lingua, titolo;
	private boolean bozza, letto;
        private SistemaCifratura sdc;
        private int idMitt, idDest;
    
    public Messaggio(int idMitt, int idDest) throws SQLException {
        id = -1;
        testo = "";
        testoCifrato = "";
        lingua = "";
        titolo = "";
        bozza = true;
        letto = false;
        Proposta propAttiva = Proposta.caricaAttiva(idMitt, idDest);
        sdc = propAttiva.getSistemaCifratura();
        sdc.calcolaMappatura();
        this.idMitt = idMitt;
        this.idDest = idDest;
    }    
        
    public Messaggio(CachedRowSet rs) throws SQLException {
        id = rs.getInt("id");
        Clob clob = rs.getClob("testo");
        testo = clob.getSubString(1, (int) clob.length());
        clob = rs.getClob("testoCifrato");
        testoCifrato = clob.getSubString(1, (int) clob.length());
        lingua = rs.getString("lingua");
        titolo = rs.getString("titolo");
        bozza = rs.getBoolean("bozza");
        letto = rs.getBoolean("letto");
        sdc = SistemaCifratura.load(rs.getInt("sistemaCifratura"));
        sdc.calcolaMappatura();
        idMitt = rs.getInt("mittente");
        idDest = rs.getInt("destinatario");
    }

    @Override
    public boolean isBozza() {
        return bozza;
    }
    
    @Override
    public boolean send() throws SQLException {
        if(sdc.prova(testo).equals(testoCifrato)) {
            bozza = false;
            save();
            return true;
        }
        return false;
    }

    @Override
    public boolean save() {
        if(sdc.prova(testo).equals(testoCifrato)) {
            if(id != -1)
                DBController.executeUpdate("UPDATE MESSAGGIO SET testo = ?, testoCifrato = ?, lingua = ?, titolo = ?, bozza = ?, letto = ?, sistemaCifratura = ?"
                        + "WHERE id = ?", testo, testoCifrato, lingua, titolo, bozza, letto, sdc.getId(), id);
            else {
                this.id = DBController.executeUpdateGenKey("INSERT INTO MESSAGGIO(testo,testoCifrato,lingua,titolo,bozza,letto,mittente,destinatario,sistemaCifratura)"
                        + "VALUES(?,?,?,?,?,?,?,?,?)", testo, testoCifrato, lingua, titolo, bozza, letto, idMitt, idDest, sdc.getId());
            }
            return true;
        }
        return false;
    }

    @Override
    public void cifra() {
        try {
            this.sdc = Proposta.caricaAttiva(idMitt, idDest).getSistemaCifratura();
            sdc.calcolaMappatura();
            this.testoCifrato = sdc.prova(testo);
        } catch(SQLException e) {
            System.out.println(e.getErrorCode());
        }
    }

    @Override
    public String getTesto() {
        return testo;
    }
    
    @Override
    public void setTesto(String testo) {
        this.testo = testo;
    }

    @Override
    public String getTestoCifrato() {
        return testoCifrato;
    }

    @Override
    public String getLingua() {
        return lingua;
    }
    
    @Override
    public void setLingua(String lingua) {
        this.lingua = lingua;
    }

    @Override
    public String getTitolo() {
        return titolo;
    }
    
    @Override
    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    @Override
    public boolean elimina() {
        if(id != -1) {
            DBController.executeUpdate("DELETE FROM MESSAGGIO WHERE id = ?", this.id);
            return true;
        }
        return false;
    }

    @Override
    public boolean isLetto() {
        return letto;
    }
    
    @Override
    public void letto() {
        this.letto = true;
        save();
    }

    @Override
    public void setDest(int idDest) {
        this.idDest = idDest;
    }
    
    @Override
    public int getDest() {
        return idDest;
    }
    
    @Override
    public String toString() {
        String res = "";
        try {
            res = "To: " + UserInfo.load(idDest) + " From: " + UserInfo.load(idMitt) + " Titolo: " + this.titolo;
        } catch(SQLException e) {
            System.out.println(e.getErrorCode());
        }
        return res;
    }
    
    public static Messaggio load(int id) {
        try(CachedRowSet rs = DBController.executeQuery("SELECT * FROM MESSAGGIO WHERE id = ?", id)) {
            return (rs.next()) ? new Messaggio(rs) : null;
        } catch(SQLException e) {
            System.out.println(e.getErrorCode());
            return null;
        }
    }
    
    public static List<MessaggioMittente> caricaInviati(UserInfo s) throws SQLException {
        List<MessaggioMittente> messages = new ArrayList<>();
        CachedRowSet rs = DBController.executeQuery("SELECT * FROM MESSAGGIO WHERE mittente = ? and bozza = 0", s.getId());
        if(rs != null)
            while(rs.next())
                messages.add(new Messaggio(rs));
        return messages;
    }
    
    public static List<MessaggioMittente> caricaBozze(UserInfo s) throws SQLException {
        List<MessaggioMittente> messages = new ArrayList<>();
        CachedRowSet rs = DBController.executeQuery("SELECT * FROM MESSAGGIO WHERE mittente = ? and bozza = 1", s.getId());
        if(rs != null)
            while(rs.next())
                messages.add(new Messaggio(rs));
        return messages;
    }
    
    public static List<MessaggioDestinatario> caricaRicevuti(UserInfo s) throws SQLException {
        List<MessaggioDestinatario> messages = new ArrayList<>();
        CachedRowSet rs = DBController.executeQuery("SELECT * FROM MESSAGGIO WHERE destinatario = ? and bozza = 0", s.getId());
        if(rs != null)
            while(rs.next())
                messages.add(new Messaggio(rs));
        return messages;
    }
}