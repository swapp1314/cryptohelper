package it.unito.di.cryptoware.cryptohelper;

public class CalcolatoreCesare extends CalcolatoreMappatura {

    @Override
    public Mappatura calcola(String chiave) {
        int n = Integer.parseInt(chiave);
        char mappa[] = new char[26];
        for(int i=0; i<mappa.length; i++) {
            mappa[i] = (char)((n+i)%26+'a');
        }
        return new Mappatura(mappa);
    }
}
