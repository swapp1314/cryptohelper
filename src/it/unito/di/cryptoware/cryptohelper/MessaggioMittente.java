package it.unito.di.cryptoware.cryptohelper;

import java.sql.SQLException;

public interface MessaggioMittente extends MessaggioAstratto {
	public boolean isBozza();
        public void setTesto(String testo);
        public void setLingua(String lingua);
        public void setTitolo(String titolo);
        public void setDest(int idDest);
        public int getDest();
	public boolean save();
        public boolean send() throws SQLException;
	public void cifra();
}