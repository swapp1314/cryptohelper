package it.unito.di.cryptoware.cryptohelper;

import java.text.Normalizer;

public class Cifratore {
    public static String cifraMonoalfabetica(Mappatura mappa, String testo) {
        String res = "";
        testo = Normalizer.normalize(testo, Normalizer.Form.NFD).replaceAll("[^a-zA-Z]", "");
        for (char c : testo.toCharArray()) {
            res += mappa.map(c);
        }
        return res;
    }
    
    public static String decifraMonoalfabetica(Mappatura mappa, String testo) {
        String res = "";
        
        for (char c : testo.toCharArray()) {
            res += mappa.inverseMap(c);
        }
        return res;
    }
}
