package it.unito.di.cryptoware.cryptohelper.auth;

import it.unito.di.cryptoware.cryptohelper.DBController;
import it.unito.di.cryptoware.cryptohelper.Studente;
import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;

public class LoginController {
    
    /**
     * User authentication
     * @param user the username
     * @param pass the password
     * @return Studente bean if the authentication succeeds, null otherwise
     */
    public static Studente authenticate(String user, String pass) {
        try (CachedRowSet rs = DBController.executeQuery("SELECT * FROM STUDENTE WHERE nickname = ? and password = ?", user, pass)) {
            return (rs != null && rs.next()) ? new Studente(rs) : null;
        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
            return null;
        }
    }
}
