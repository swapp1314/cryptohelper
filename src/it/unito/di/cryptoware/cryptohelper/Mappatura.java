package it.unito.di.cryptoware.cryptohelper;

public class Mappatura {
    private char mappa[], mappaInversa[];
    
    public Mappatura(char m[]) {
        mappa = new char[26];
        mappaInversa = new char[26];
        for(int i=0; i<m.length; i++) {
            mappa[i] = m[i];
            mappaInversa[m[i]-'a'] = (char)(i + 'a');
        }
    }
    
    public char map(char c){
        char res = mappa[Character.toLowerCase(c) - 'a'];
        return Character.isUpperCase(c) ? Character.toUpperCase(res) : res;
    }
    
    public char inverseMap(char c){
        char res = mappaInversa[Character.toLowerCase(c) - 'a'];
        return Character.isUpperCase(c) ? Character.toUpperCase(res) : res;
    }
}
