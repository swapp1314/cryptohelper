package it.unito.di.cryptoware.cryptohelper;

import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;

public class Proposta {
    private int id;
    private String stato;
    private boolean notificata;
    private SistemaCifratura sdc;
    private UserInfo proponente, partner;
    
    public Proposta(UserInfo user, UserInfo partner, SistemaCifratura sdc) {
        this.id = -1;
        this.proponente = user;
        this.partner = partner;
        this.sdc = sdc;
        this.stato = "pending";
        this.notificata = false;
    }
    
    public Proposta(CachedRowSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.proponente = UserInfo.load(rs.getInt("proponente"));
        this.partner = UserInfo.load(rs.getInt("partner"));
        this.sdc = SistemaCifratura.load(rs.getInt("sistemaCifratura"));
        this.stato = rs.getString("stato");
        this.notificata = rs.getBoolean("notificata");
    }

    public void setStato(String stato) { this.stato = stato; }
    public void setNotificata(boolean notificata) { this.notificata = notificata; }

    public String getStato() { return stato; }
    public boolean getNotificata() { return notificata; }
    public UserInfo getProponente() { return proponente; }
    public UserInfo getPartner() { return partner; }
    public SistemaCifratura getSistemaCifratura() { return sdc; }

    public void save() throws SQLException {
        if(id != -1)
            DBController.executeUpdate("UPDATE PROPOSTA SET stato = ?, notificata = ?"
                    + "WHERE id = ?", stato, notificata, id);
        else {
            sdc.save();
            this.id = DBController.executeUpdateGenKey("INSERT INTO PROPOSTA(stato,notificata,proponente,partner,sistemaCifratura)"
                    + "VALUES(?,?,?,?,?)", stato, notificata, proponente.getId(), partner.getId(), sdc.getId());
        }
    }

    public static Proposta caricaAttiva(int idProp, int idPartner) throws SQLException {
        CachedRowSet rs = DBController.executeQuery("SELECT * FROM PROPOSTA WHERE ((proponente = ? and partner = ?) or (proponente = ? and partner = ?)) "
                + "and stato = 'accepted'", idProp, idPartner, idPartner, idProp);
        return (rs != null && rs.next()) ? new Proposta(rs) : null;
    }
}
