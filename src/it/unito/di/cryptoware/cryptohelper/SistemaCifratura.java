package it.unito.di.cryptoware.cryptohelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.management.BadStringOperationException;
import javax.sql.rowset.CachedRowSet;

public class SistemaCifratura {
    private int id;
    private String chiave, metodo;
    private Mappatura map;
    private int idCreatore;
    
    public static List<SistemaCifratura> caricaSistemiCifratura(UserInfo s) throws SQLException {
        CachedRowSet rs = DBController.executeQuery("SELECT metodo,chiave FROM SISTEMACIFRATURA WHERE creatore = ?", s.getId());
        List<SistemaCifratura> l = new ArrayList<>();
        while(rs.next()) {
            SistemaCifratura curr = new SistemaCifratura(rs);
            l.add(curr);
        }
        return l;
    }
    
    public static SistemaCifratura load(int id) throws SQLException {
        CachedRowSet rs = DBController.executeQuery("SELECT id,metodo,chiave,creatore FROM SISTEMACIFRATURA WHERE id = ?",id);
        return (rs != null && rs.next()) ? new SistemaCifratura(rs) : null;
    }
    
    public SistemaCifratura(String chiave, String metodo, int creatore) {
        this.chiave = chiave;
        this.metodo = metodo;
        this.idCreatore = creatore;
    }
    
    public SistemaCifratura(CachedRowSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.chiave = rs.getString("chiave");
        this.metodo = rs.getString("metodo");
        this.idCreatore = rs.getInt("creatore");
    }
    
    public int getId() { return id; }
    public String getChiave() { return chiave; }
    public String getMetodo() { return metodo; }
    
    public String prova(String testo) {
        return Cifratore.cifraMonoalfabetica(map,testo);
    }
    
    public void calcolaMappatura() {
        try {
            map = CalcolatoreMappatura.create(metodo).calcola(chiave);
        } catch(BadStringOperationException e) {
            System.out.println(e);
        }
    }
    
    public void save() {
        this.id = DBController.executeUpdateGenKey("INSERT INTO SISTEMACIFRATURA(metodo,chiave,creatore) VALUES(?,?,?)",metodo,chiave,idCreatore);
    }
}
