package it.unito.di.cryptoware.cryptohelper;

import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;

public class UserInfo {
    private int id;
    private String nome, cognome;
     
    public UserInfo() {}
    public UserInfo(Studente s) {
        id = s.getId();
        nome = s.getNome();
        cognome = s.getCognome();
    }
    
    public UserInfo(CachedRowSet rs) throws SQLException {
        id = rs.getInt("id");
        nome = rs.getString("nome");
        cognome = rs.getString("cognome");
    }

    public int getId() {
        return id;
    }
    
    public String getNome() {
        return nome;
    }
    
    public String getCognome() {
        return cognome;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return nome + " " + cognome;
    }
    
    public static UserInfo load(int id) throws SQLException {
        CachedRowSet rs = DBController.executeQuery("SELECT id,nome,cognome FROM STUDENTE WHERE id = ?",id);
        return (rs != null && rs.next()) ? new UserInfo(rs) : null;
    }
}