package it.unito.di.cryptoware.cryptohelper;

import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;

public class Studente {
    private int id;
    private String nickname, password, nome, cognome;
    
    public Studente(int i, String n, String p, String nome, String cognome) {
        this.id = i;
        this.nickname = n;
        this.password = p;
        this.nome = nome;
        this.cognome = cognome;
    }
    
    public Studente(CachedRowSet rs) {
        try {
            this.id = rs.getInt("id");
            this.nickname = rs.getString("nickname");
            this.password = rs.getString("password");
            this.nome = rs.getString("nome");
            this.cognome = rs.getString("cognome");
        } catch (SQLException e) {
           System.out.println(e.getErrorCode());
        }
    }
    
    public int getId() {
        return id;
    }
    
    public String getNickname() {
        return nickname;
    }
    
    public String getNome() {
        return nome;
    }
    
    public String getCognome() {
        return cognome;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
}
