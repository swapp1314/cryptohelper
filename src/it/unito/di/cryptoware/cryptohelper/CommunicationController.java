package it.unito.di.cryptoware.cryptohelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;

public class CommunicationController {

    public static void inviaProposta(UserInfo user, UserInfo partner, SistemaCifratura sdc) throws SQLException {
        CachedRowSet rs = DBController.executeQuery("SELECT id,sistemaCifratura FROM PROPOSTA WHERE proponente = ? and partner = ? and stato = 'pending'", user.getId(), partner.getId());
        if(rs != null && rs.next()){
            DBController.executeUpdate("DELETE FROM PROPOSTA WHERE id = ?", rs.getInt("id"));
            DBController.executeUpdate("DELETE FROM SISTEMACIFRATURA WHERE id = ?", rs.getInt("sistemaCifratura"));
        }
        Proposta p = new Proposta(user,partner,sdc);
        p.save();
    }

    public static List<Proposta> getProposte(UserInfo user) throws SQLException {
        List<Proposta> proposte = new ArrayList<>();
        CachedRowSet rs = DBController.executeQuery("SELECT * FROM PROPOSTA WHERE partner = ? and stato = 'pending'", user.getId());
        if(rs != null) {
            while(rs.next()) {
                proposte.add(new Proposta(rs));
            }
        }
        return proposte;
    }

    public static void inviaDecisione(Proposta proposta, String decisione) throws SQLException {
        System.out.println("Nuova proposta(prima), prop: " + proposta.getProponente() + " partner: " + proposta.getPartner() + "stato: " + proposta.getStato());
        if(decisione.equals("accepted")) {
            Proposta old = Proposta.caricaAttiva(proposta.getProponente().getId(), proposta.getPartner().getId());
            if(old != null) {
                old.setStato("expired");
                old.save();
                System.out.println("Vecchia proposta, prop: " + old.getProponente() + " partner: " + old.getPartner() + "stato: " + old.getStato());
            }
        }
        proposta.setStato(decisione);
        proposta.save();
        System.out.println("Nuova proposta(dopo), prop: " + proposta.getProponente() + " partner: " + proposta.getPartner() + "stato: " + proposta.getStato());
    }

    public static List<Proposta> getAccettazioneProposte(UserInfo user) throws SQLException {
        List<Proposta> proposte = new ArrayList<>();
        CachedRowSet rs = DBController.executeQuery("SELECT * FROM PROPOSTA WHERE proponente = ? and (stato = 'accepted' or stato = 'refused')"
                + "and notificata = 0", user.getId());
        if(rs != null) {
            while(rs.next()) {
                Proposta p = new Proposta(rs);
                p.setNotificata(true);
                p.save();
                proposte.add(p);
            }
        }
        return proposte;
    }

    public static List<UserInfo> getDestinatari(UserInfo studente) throws SQLException {
        List<UserInfo> dest = new ArrayList<>();
        CachedRowSet rs = DBController.executeQuery("SELECT STUDENTE.* FROM STUDENTE,PROPOSTA\n"
                + "WHERE ((proponente = studente.id and partner = ?) or (partner = studente.id and proponente = ?)) and stato = 'accepted'",
                studente.getId(), studente.getId());
        if(rs !=  null)
            while(rs.next())
                dest.add(new UserInfo(rs));
        return dest;
    }

    public static List<UserInfo> getStudenti(UserInfo studente) throws SQLException {
        List<UserInfo> st = new ArrayList<>();
        CachedRowSet rs = DBController.executeQuery("SELECT * FROM STUDENTE WHERE id <> ?", studente.getId());
        if(rs != null)
            while(rs.next())
                st.add(new UserInfo(rs));
        return st;
    }
    public static void send(MessaggioMittente messaggio) throws SQLException {
        messaggio.send();
    }
    public static MessaggioDestinatario apriMessaggioRicevuto(int id) throws SQLException {
        MessaggioDestinatario mex = Messaggio.load(id);
        mex.letto();
        return mex;
    }
}