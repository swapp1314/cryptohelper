package it.unito.di.cryptoware.cryptohelper.gui;

import it.unito.di.cryptoware.cryptohelper.UserInfo;

public abstract class LoggedUserPanel extends javax.swing.JPanel {
    public abstract void setLoggedUser(UserInfo user);
}