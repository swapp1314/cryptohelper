package it.unito.di.cryptoware.cryptohelper.gui;

import it.unito.di.cryptoware.cryptohelper.*;
import java.awt.Color;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListModel;

public class NotifyPropPanel extends LoggedUserPanel {
    private UserInfo user;
    private DefaultListModel<UserInfo> model;
    private Map<UserInfo,Proposta> proposte;
    private UserInfo selected;

    /**
     * Creates new form NotifyPanel
     */
    public NotifyPropPanel() {
        this.model = new DefaultListModel<>();
        this.proposte = new HashMap<>();
        this.selected = null;
        initComponents();
    }
    
    @Override
    public void setLoggedUser(UserInfo user) {
        this.user = user;
        this.list.clearSelection();
        this.model.removeAllElements();
        this.proposte.clear();
        this.selected = null;
        this.methLbl.setText("");
        this.keyLbl.setText("");
        this.decLbl.setText("");
        this.info.setText("");
        try {
            for(Proposta p : CommunicationController.getAccettazioneProposte(user)) {
                proposte.put(p.getPartner(), p);
                model.addElement(p.getPartner());
            }
        } catch(SQLException e) {
            info.setForeground(Color.RED);
            info.setText("Errore download proposte!");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        infoPanel = new javax.swing.JPanel();
        info = new javax.swing.JLabel();
        confWrapper = new javax.swing.JPanel();
        scrollList = new javax.swing.JScrollPane();
        list = new javax.swing.JList(model);
        labelsWrapper = new javax.swing.JPanel();
        methLbl = new javax.swing.JLabel();
        keyLbl = new javax.swing.JLabel();
        decLbl = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout infoPanelLayout = new javax.swing.GroupLayout(infoPanel);
        infoPanel.setLayout(infoPanelLayout);
        infoPanelLayout.setHorizontalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(infoPanelLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(info)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        infoPanelLayout.setVerticalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(infoPanelLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(info)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        add(infoPanel, java.awt.BorderLayout.NORTH);

        confWrapper.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        list.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listValueChanged(evt);
            }
        });
        scrollList.setViewportView(list);

        confWrapper.add(scrollList);

        labelsWrapper.setLayout(new javax.swing.BoxLayout(labelsWrapper, javax.swing.BoxLayout.PAGE_AXIS));
        labelsWrapper.add(methLbl);
        labelsWrapper.add(keyLbl);
        labelsWrapper.add(decLbl);

        confWrapper.add(labelsWrapper);

        add(confWrapper, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void listValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listValueChanged
        selected = (UserInfo)list.getSelectedValue();
        if(selected != null) {
            SistemaCifratura sdc = proposte.get(selected).getSistemaCifratura();
            methLbl.setText("Metodo: " + sdc.getMetodo());
            keyLbl.setText("Chiave: " + sdc.getChiave());
            boolean accettato = proposte.get(selected).getStato().equals("accepted");
            decLbl.setText("Accettata: " + (accettato ? "Sì" : "No"));
        } else {
            methLbl.setText("");
            keyLbl.setText("");
            decLbl.setText("");
        }
    }//GEN-LAST:event_listValueChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel confWrapper;
    private javax.swing.JLabel decLbl;
    private javax.swing.JLabel info;
    private javax.swing.JPanel infoPanel;
    private javax.swing.JLabel keyLbl;
    private javax.swing.JPanel labelsWrapper;
    private javax.swing.JList list;
    private javax.swing.JLabel methLbl;
    private javax.swing.JScrollPane scrollList;
    // End of variables declaration//GEN-END:variables
}
