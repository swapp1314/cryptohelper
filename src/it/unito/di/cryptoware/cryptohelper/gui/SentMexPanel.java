package it.unito.di.cryptoware.cryptohelper.gui;

import it.unito.di.cryptoware.cryptohelper.Messaggio;
import it.unito.di.cryptoware.cryptohelper.MessaggioMittente;
import it.unito.di.cryptoware.cryptohelper.UserInfo;
import java.sql.SQLException;
import javax.swing.DefaultListModel;


public class SentMexPanel extends LoggedUserPanel {
    
    private UserInfo user;
    private DefaultListModel<MessaggioMittente> inviati;

    public SentMexPanel() {
        this.inviati = new DefaultListModel<>();
        initComponents();
    }
    
    @Override
    public void setLoggedUser(UserInfo user) {
        this.user = user;
        lingua.setText("");
        testoTxt.setText("");
        testoCifratoTxt.setText("");
        inviati.removeAllElements();
        try {
            for(MessaggioMittente mex : Messaggio.caricaInviati(user)) {
                    inviati.addElement(mex);
            }
        } catch(SQLException e) {
            System.out.println(e.getErrorCode());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        newLbl = new javax.swing.JLabel();
        scrollPaneSent = new javax.swing.JScrollPane();
        listSent = new javax.swing.JList(inviati);
        linguaLbl = new javax.swing.JLabel();
        lingua = new javax.swing.JLabel();
        testoLbl = new javax.swing.JLabel();
        testoScrollPane = new javax.swing.JScrollPane();
        testoTxt = new javax.swing.JTextArea();
        testoCifratoLbl = new javax.swing.JLabel();
        testoCifratoScrollPane = new javax.swing.JScrollPane();
        testoCifratoTxt = new javax.swing.JTextArea();

        setLayout(new java.awt.GridBagLayout());

        newLbl.setText("Inviati");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        add(newLbl, gridBagConstraints);

        scrollPaneSent.setPreferredSize(new java.awt.Dimension(369, 166));

        listSent.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listSentValueChanged(evt);
            }
        });
        scrollPaneSent.setViewportView(listSent);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 4;
        add(scrollPaneSent, gridBagConstraints);

        linguaLbl.setText("Lingua");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(linguaLbl, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        add(lingua, gridBagConstraints);

        testoLbl.setText("Testo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(testoLbl, gridBagConstraints);

        testoTxt.setEditable(false);
        testoTxt.setColumns(22);
        testoTxt.setLineWrap(true);
        testoTxt.setRows(5);
        testoScrollPane.setViewportView(testoTxt);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(testoScrollPane, gridBagConstraints);

        testoCifratoLbl.setText("Testo cifrato");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(testoCifratoLbl, gridBagConstraints);

        testoCifratoTxt.setEditable(false);
        testoCifratoTxt.setColumns(22);
        testoCifratoTxt.setLineWrap(true);
        testoCifratoTxt.setRows(5);
        testoCifratoScrollPane.setViewportView(testoCifratoTxt);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(testoCifratoScrollPane, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void listSentValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listSentValueChanged
        MessaggioMittente mex = (MessaggioMittente) listSent.getSelectedValue();
        if(mex != null) {
            lingua.setText(mex.getLingua());
            testoTxt.setText(mex.getTesto());
            testoCifratoTxt.setText(mex.getTestoCifrato());
        }
    }//GEN-LAST:event_listSentValueChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lingua;
    private javax.swing.JLabel linguaLbl;
    private javax.swing.JList listSent;
    private javax.swing.JLabel newLbl;
    private javax.swing.JScrollPane scrollPaneSent;
    private javax.swing.JLabel testoCifratoLbl;
    private javax.swing.JScrollPane testoCifratoScrollPane;
    private javax.swing.JTextArea testoCifratoTxt;
    private javax.swing.JLabel testoLbl;
    private javax.swing.JScrollPane testoScrollPane;
    private javax.swing.JTextArea testoTxt;
    // End of variables declaration//GEN-END:variables
}
