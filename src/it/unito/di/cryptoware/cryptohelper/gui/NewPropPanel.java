package it.unito.di.cryptoware.cryptohelper.gui;

import it.unito.di.cryptoware.cryptohelper.*;
import java.awt.Color;
import java.sql.SQLException;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.DefaultListModel;

public class NewPropPanel extends LoggedUserPanel {
    private UserInfo user;
    private DefaultListModel<UserInfo> model;

    /**
     * Creates new form NewPropPanel
     */
    public NewPropPanel() {
        model = new DefaultListModel<>();
        initComponents();
    }
    
    public void setLoggedUser(UserInfo user) {
        this.user = user;
        this.list.clearSelection();
        this.model.removeAllElements();
        this.methGroup.clearSelection();
        this.keyTxt.setText("");
        this.info.setText("");
        try {
            for(UserInfo u : CommunicationController.getStudenti(user))
                model.addElement(u);
        } catch(SQLException e) {
            info.setForeground(Color.RED);
            info.setText("Errore download utenti!");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        methGroup = new javax.swing.ButtonGroup();
        infoPanel = new javax.swing.JPanel();
        info = new javax.swing.JLabel();
        confWrapper = new javax.swing.JPanel();
        scrollList = new javax.swing.JScrollPane();
        list = new javax.swing.JList(model);
        choiceWrapper = new javax.swing.JPanel();
        keyBtn = new javax.swing.JRadioButton();
        caesarBtn = new javax.swing.JRadioButton();
        pseudoBtn = new javax.swing.JRadioButton();
        keyTxt = new javax.swing.JTextField();
        btnWrapper = new javax.swing.JPanel();
        sendBtn = new javax.swing.JButton();
        resetBtn = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout infoPanelLayout = new javax.swing.GroupLayout(infoPanel);
        infoPanel.setLayout(infoPanelLayout);
        infoPanelLayout.setHorizontalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(infoPanelLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(info)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        infoPanelLayout.setVerticalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(infoPanelLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(info)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        add(infoPanel, java.awt.BorderLayout.NORTH);

        confWrapper.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        scrollList.setViewportView(list);

        confWrapper.add(scrollList);

        choiceWrapper.setLayout(new javax.swing.BoxLayout(choiceWrapper, javax.swing.BoxLayout.PAGE_AXIS));

        keyBtn.setText("Parola chiave");
        choiceWrapper.add(keyBtn);
        methGroup.add(keyBtn);

        caesarBtn.setText("Cesare");
        choiceWrapper.add(caesarBtn);
        methGroup.add(caesarBtn);

        pseudoBtn.setText("Pseudocasuale");
        choiceWrapper.add(pseudoBtn);
        methGroup.add(pseudoBtn);

        confWrapper.add(choiceWrapper);

        keyTxt.setColumns(10);
        confWrapper.add(keyTxt);

        add(confWrapper, java.awt.BorderLayout.CENTER);

        btnWrapper.setLayout(new javax.swing.BoxLayout(btnWrapper, javax.swing.BoxLayout.LINE_AXIS));

        sendBtn.setText("Invia");
        sendBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendBtnActionPerformed(evt);
            }
        });
        btnWrapper.add(sendBtn);

        resetBtn.setText("Reset");
        resetBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetBtnActionPerformed(evt);
            }
        });
        btnWrapper.add(resetBtn);

        add(btnWrapper, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents

    private void sendBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendBtnActionPerformed
        UserInfo partner = (UserInfo)list.getSelectedValue();
        String method = "";
        for(Enumeration<AbstractButton> buttons = methGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton i = buttons.nextElement();
            if(i.isSelected()) method = i.getText();
        }
        String key = keyTxt.getText();
        if(validation(method,key))
            try {
                CommunicationController.inviaProposta(user, partner, new SistemaCifratura(key,method.toLowerCase(),user.getId()));
            } catch(SQLException e) {
                info.setForeground(Color.RED);
                info.setText("Errore invio proposta!");
            } finally {
                info.setForeground(Color.BLUE);
                info.setText("Proposta inviata correttamente!");
            }
        else {
            info.setForeground(Color.RED);
            info.setText("Errore inserimento valori!");
        }
    }//GEN-LAST:event_sendBtnActionPerformed

    public boolean validation(String method, String key) {
        switch(method.toLowerCase()) {
            case "parola chiave":
                return key.matches("[A-Za-z]{1,26}");
            case "cesare":
            case "pseudocasuale":
                return key.matches("[0-9]+");
                
        }
        return false;
    }

    private void resetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetBtnActionPerformed
        list.clearSelection();
        methGroup.clearSelection();
        keyTxt.setText("");
        info.setText("");
    }//GEN-LAST:event_resetBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel btnWrapper;
    private javax.swing.JRadioButton caesarBtn;
    private javax.swing.JPanel choiceWrapper;
    private javax.swing.JPanel confWrapper;
    private javax.swing.JLabel info;
    private javax.swing.JPanel infoPanel;
    private javax.swing.JRadioButton keyBtn;
    private javax.swing.JTextField keyTxt;
    private javax.swing.JList list;
    private javax.swing.ButtonGroup methGroup;
    private javax.swing.JRadioButton pseudoBtn;
    private javax.swing.JButton resetBtn;
    private javax.swing.JScrollPane scrollList;
    private javax.swing.JButton sendBtn;
    // End of variables declaration//GEN-END:variables
}
