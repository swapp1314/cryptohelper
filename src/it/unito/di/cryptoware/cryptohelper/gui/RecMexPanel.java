package it.unito.di.cryptoware.cryptohelper.gui;

import it.unito.di.cryptoware.cryptohelper.Messaggio;
import it.unito.di.cryptoware.cryptohelper.MessaggioDestinatario;
import it.unito.di.cryptoware.cryptohelper.UserInfo;
import java.sql.SQLException;
import javax.swing.DefaultListModel;


public class RecMexPanel extends LoggedUserPanel {
    
    private UserInfo user;
    private DefaultListModel<MessaggioDestinatario> nuovi;
    private DefaultListModel<MessaggioDestinatario> letti;

    public RecMexPanel() {
        this.nuovi = new DefaultListModel<>();
        this.letti = new DefaultListModel<>();
        initComponents();
    }
    
    @Override
    public void setLoggedUser(UserInfo user) {
        this.user = user;
        lingua.setText("");
        testoTxt.setText("");
        testoCifratoTxt.setText("");
        nuovi.removeAllElements();
        letti.removeAllElements();
        try {
            for(MessaggioDestinatario mex : Messaggio.caricaRicevuti(user)) {
                if(mex.isLetto())
                    letti.addElement(mex);
                else
                    nuovi.addElement(mex);
            }
        } catch(SQLException e) {
            System.out.println(e.getErrorCode());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        newLbl = new javax.swing.JLabel();
        scrollPaneNew = new javax.swing.JScrollPane();
        listNew = new javax.swing.JList(nuovi);
        readLbl = new javax.swing.JLabel();
        scrollPaneRead = new javax.swing.JScrollPane();
        listRead = new javax.swing.JList(letti);
        linguaLbl = new javax.swing.JLabel();
        lingua = new javax.swing.JLabel();
        testoLbl = new javax.swing.JLabel();
        testoScrollPane = new javax.swing.JScrollPane();
        testoTxt = new javax.swing.JTextArea();
        testoCifratoLbl = new javax.swing.JLabel();
        testoCifratoScrollPane = new javax.swing.JScrollPane();
        testoCifratoTxt = new javax.swing.JTextArea();

        setLayout(new java.awt.GridBagLayout());

        newLbl.setText("Nuovi");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        add(newLbl, gridBagConstraints);

        scrollPaneNew.setPreferredSize(new java.awt.Dimension(369, 166));

        listNew.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listNewValueChanged(evt);
            }
        });
        scrollPaneNew.setViewportView(listNew);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        add(scrollPaneNew, gridBagConstraints);

        readLbl.setText("Letti");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        add(readLbl, gridBagConstraints);

        scrollPaneRead.setPreferredSize(new java.awt.Dimension(369, 166));

        listRead.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listReadValueChanged(evt);
            }
        });
        scrollPaneRead.setViewportView(listRead);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        add(scrollPaneRead, gridBagConstraints);

        linguaLbl.setText("Lingua");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(linguaLbl, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        add(lingua, gridBagConstraints);

        testoLbl.setText("Testo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(testoLbl, gridBagConstraints);

        testoTxt.setEditable(false);
        testoTxt.setColumns(22);
        testoTxt.setLineWrap(true);
        testoTxt.setRows(5);
        testoScrollPane.setViewportView(testoTxt);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(testoScrollPane, gridBagConstraints);

        testoCifratoLbl.setText("Testo cifrato");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(testoCifratoLbl, gridBagConstraints);

        testoCifratoTxt.setEditable(false);
        testoCifratoTxt.setColumns(22);
        testoCifratoTxt.setLineWrap(true);
        testoCifratoTxt.setRows(5);
        testoCifratoScrollPane.setViewportView(testoCifratoTxt);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(testoCifratoScrollPane, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void listNewValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listNewValueChanged
        MessaggioDestinatario mex = (MessaggioDestinatario) listNew.getSelectedValue();
        if(mex != null) {
            lingua.setText(mex.getLingua());
            testoTxt.setText(mex.getTesto());
            testoCifratoTxt.setText(mex.getTestoCifrato());
            mex.letto();
        }
    }//GEN-LAST:event_listNewValueChanged

    private void listReadValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listReadValueChanged
        MessaggioDestinatario mex = (MessaggioDestinatario) listRead.getSelectedValue();
        if(mex != null) {
            lingua.setText(mex.getLingua());
            testoTxt.setText(mex.getTesto());
            testoCifratoTxt.setText(mex.getTestoCifrato());
        }
    }//GEN-LAST:event_listReadValueChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lingua;
    private javax.swing.JLabel linguaLbl;
    private javax.swing.JList listNew;
    private javax.swing.JList listRead;
    private javax.swing.JLabel newLbl;
    private javax.swing.JLabel readLbl;
    private javax.swing.JScrollPane scrollPaneNew;
    private javax.swing.JScrollPane scrollPaneRead;
    private javax.swing.JLabel testoCifratoLbl;
    private javax.swing.JScrollPane testoCifratoScrollPane;
    private javax.swing.JTextArea testoCifratoTxt;
    private javax.swing.JLabel testoLbl;
    private javax.swing.JScrollPane testoScrollPane;
    private javax.swing.JTextArea testoTxt;
    // End of variables declaration//GEN-END:variables
}
