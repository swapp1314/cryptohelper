package it.unito.di.cryptoware.cryptohelper.gui;

import it.unito.di.cryptoware.cryptohelper.Studente;
import it.unito.di.cryptoware.cryptohelper.auth.*;

public class GUIController {
    private CryptohelperFrame frame;
    private LoginDialog dialog;
    
    public GUIController() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CryptohelperFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CryptohelperFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CryptohelperFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CryptohelperFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                createDialog();
            }
        });
    }

    public void login(String user, String pass) {
        Studente s = LoginController.authenticate(user, pass);
        if (s != null) {
            dialog.dispose();
            createFrame(s);
        } else {
            dialog.setError("Username e/o password errati");
        }
    }
    
    public void logout() {
        frame.dispose();
        createDialog();
    }
    
    
    public void createFrame(Studente s) {
        frame = new CryptohelperFrame(this);
        frame.setLoggedUser(s);
        frame.setVisible(true);
    }

    public void createDialog() {
        dialog = new LoginDialog(this);
        dialog.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                System.exit(0);
            }
        });
        dialog.setVisible(true);
    }
}
