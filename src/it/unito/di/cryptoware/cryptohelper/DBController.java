package it.unito.di.cryptoware.cryptohelper;

import java.sql.*;
import javax.sql.rowset.CachedRowSet;
import com.sun.rowset.CachedRowSetImpl;

public class DBController {
    private static final String url = "jdbc:derby://localhost:1527/sviluppo";
    private static final String user = "cryptoware";
    private static final String pass = "cryptoware";

    
    private static Connection dbConnect() throws SQLException {
        DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
        return DriverManager.getConnection(url,user,pass);
    }
    
    /**
     * Executes an SQL query and returns a CachedRowSet
     * @param query SQL query string to be passed to the PreparedStatement
     * @param args  parameterized arguments
     * @return  the offline CachedRowSet resulting from the execution of the query, null in case of error
     */
    
    public static CachedRowSet executeQuery(String query, Object ... args) {
        CachedRowSet set = null;
        try (PreparedStatement st = dbConnect().prepareStatement(query)) {
            for(int i=0; i<args.length; i++) {
                if(args[i] == null)
                    st.setNull(i+1,java.sql.Types.INTEGER);
                else
                    st.setObject(i+1, args[i]);
            }
            ResultSet rs = st.executeQuery();
            set = new CachedRowSetImpl();
            set.populate(rs);
        } catch(SQLException e) {
            System.out.println("Error code: " + e.getErrorCode());
        }
        return set;
    }

    /**
     * Executes an SQL query that doesn't return anything
     * @param query SQL query string to be passed to the PreparedStatement
     * @param args  parameterized arguments
     * @return true if the query was executed normally, false otherwise
     */
    public static boolean executeUpdate(String query, Object ... args) {
        try (PreparedStatement st = dbConnect().prepareStatement(query)) {
            for(int i=0; i<args.length; i++) {
                if(args[i] == null)
                    st.setNull(i+1,java.sql.Types.INTEGER);
                else
                    st.setObject(i+1, args[i]);
            }
            st.executeUpdate();
        } catch(SQLException e) {
            System.out.println("Error code: " + e.getErrorCode());
            return false;
        }
        return true;
    }

    /**
     * Executes an SQL query that returns an autogenerated value
     * @param query SQL query string to be passed to the PreparedStatement
     * @param args  parameterized arguments
     * @return the autogenerated value, -1 otherwise
     */
    public static int executeUpdateGenKey(String query, Object ... args) {
        try (PreparedStatement st = dbConnect().prepareStatement(query,Statement.RETURN_GENERATED_KEYS)) {
            for(int i=0; i<args.length; i++) {
                if(args[i] == null)
                    st.setNull(i+1, java.sql.Types.INTEGER);
                else
                    st.setObject(i+1, args[i]);
            }
            st.executeUpdate();
            ResultSet rs = st.getGeneratedKeys();
            rs.next();
            return rs.getInt("1");
            
        } catch(SQLException e) {
            System.out.println("Error code: " + e.getErrorCode());
            return -1;
        }
    }
}
