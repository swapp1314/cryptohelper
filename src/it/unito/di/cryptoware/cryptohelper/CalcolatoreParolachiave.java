package it.unito.di.cryptoware.cryptohelper;

public class CalcolatoreParolachiave extends CalcolatoreMappatura {
    @Override
    public Mappatura calcola(String chiave) {
        chiave = chiave.toLowerCase();
        char mappa[] = new char[26];
        for(int i=0; i<chiave.length(); i++) {
            int j=0;
            /* ciclo di controllo caratteri multipli */
            while(j<i && i<chiave.length()) {
                if(mappa[j] == chiave.charAt(i)) {
                    j=-1;
                    i++;    // ignoro i caratteri multipli
                }
                j++;
            }
            mappa[i] = chiave.charAt(i);
        }
        char cur = 'a';
        for(int i=chiave.length(); i<mappa.length; i++) {
            int j=0;
            /* ciclo di controllo caratteri già mappati */
            while(j<i) {
                if(cur == mappa[j]) {
                    j=-1;
                    cur++; // già presente, passo al prosimo
                }
                j++;
            }
            mappa[i] = cur;
            cur++;
        }
        return new Mappatura(mappa);
    } 
}
