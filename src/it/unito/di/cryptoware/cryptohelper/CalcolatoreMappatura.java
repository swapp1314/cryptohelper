package it.unito.di.cryptoware.cryptohelper;

import javax.management.BadStringOperationException;

public abstract class CalcolatoreMappatura {
    public abstract Mappatura calcola(String chiave);
    public static CalcolatoreMappatura create(String metodo) throws BadStringOperationException {
        switch(metodo.toLowerCase()) {
            case "cesare":
                return new CalcolatoreCesare();
            case "pseudocasuale":
                return new CalcolatorePseudocasuale();
            case "parola chiave":
                return new CalcolatoreParolachiave();
        }
        throw new BadStringOperationException("Metodo non presente");
    }  
}
